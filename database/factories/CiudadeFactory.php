<?php

use Faker\Generator as Faker;

$factory->define(App\Ciudade::class, function (Faker $faker) {
   return [
        'name' => $faker->unique()->city,
   
    ];
});
