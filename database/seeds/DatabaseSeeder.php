<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $role = new Role();
        $role->name = 'admin';
        $role->description = 'Administrator';
        $role->save();
        

        $role = new Role();
        $role->name = 'user';
        $role->description = 'User';
        $role->save();


        
        $role_user = Role::where('name', 'user')->first();
        $role_admin = Role::where('name', 'admin')->first();
        $user = new User();
        $user->name = 'User';
        $user->ciudade_id = 1;
        $user->paise_id = 1;
        $user->academico_id = 1;
       $user->email = 'user@example.com';
        $user->password = bcrypt('secret');
        $user->save();
        $user->roles()->attach($role_user);
        
        $user = new User();
        $user->name = 'Admin';
        $user->ciudade_id = 1;
        $user->paise_id = 1;
        $user->academico_id = 1;
        $user->email = 'admin@example.com';
        $user->password = bcrypt('secret');
        $user->save();
        $user->roles()->attach($role_admin);

        factory(App\Paise::class,100)->create();
        factory(App\Ciudade::class,200)->create();
        factory(App\Academico::class,80)->create();
        factory(App\User::class,50)->create();


    }
}
