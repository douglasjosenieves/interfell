<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ciudade extends Model
{

	protected $fillable = [
		'name',  
	];
	public function scopeName($query, $data)
	{
		$query->where('name','like','%'.$data.'%');
		return $query;
	}
}
