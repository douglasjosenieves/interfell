<?php

namespace App\Http\Controllers;

use App\Academico;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Kamaln7\Toastr\Facades\Toastr;

class AcademicoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['admin']);

        $academicos = Academico::name($request->name)->orderBy('id', 'Desc')->paginate(10);
        $data['academicos'] = $academicos;


        return view('academicos.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

     $request->user()->authorizeRoles(['admin']);
     $this->validate($request, [
        'name' => 'required|string|max:255|unique:academicos',

    ]);


     $request->merge(
        ['created_at' => Carbon::now()]
    );


     Academico::create($request->all());



     Toastr::success('Se ha agregado un nivel academico!','Buen trabajo'); 
     return redirect()->back();
 }

    /**
     * Display the specified resource.
     *
     * @param  \App\Academico  $academico
     * @return \Illuminate\Http\Response
     */
    public function show(Academico $academico)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Academico  $academico
     * @return \Illuminate\Http\Response
     */
    public function edit(Academico $academico)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Academico  $academico
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Academico $academico)
    {

        $request->user()->authorizeRoles(['admin']);
        $this->validate($request, [
            'name' => 'required|string|max:255|unique:academicos',

        ]);

        $academico->name = $request->name;
        $academico->update();
        Toastr::info('Se ha actualizado un nivel academico!','Buen trabajo'); 
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Academico  $academico
     * @return \Illuminate\Http\Response
     */
    public function destroy(Academico $academico, Request $request)
    {
        $request->user()->authorizeRoles(['admin']);
        $academico->delete();
        Toastr::error('Se ha eliminado un nivel academico!','Buen trabajo'); 
        return redirect()->back();
    }
}
