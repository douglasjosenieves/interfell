<?php

namespace App\Http\Controllers;

use App\Academico;
use App\Ciudade;
use App\Paise;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Kamaln7\Toastr\Facades\Toastr;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['admin']);

        $ciudades = Ciudade::all()->sortBy('name')->pluck('name', 'id')->prepend('Selecione ciudad', '');
        $paises = Paise::all()->sortBy('name')->pluck('name', 'id')->prepend('Selecione pais', '');
        $academicos = Academico::all()->sortBy('name')->pluck('name', 'id')->prepend('Selecione N. Academico', '');

        $users = User::name($request->name)->orderBy('id', 'Desc')->paginate(10);
        $data['users'] = $users;
        $data['ciudades'] = $ciudades;
        $data['paises'] = $paises;
        $data['academicos'] = $academicos;

        return view('users.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->user()->authorizeRoles(['admin']);

        //dd($request);
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'paise_id' => 'required',
            'ciudade_id' => 'required',
            'academico_id' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        $request->merge(
            ['created_at' => Carbon::now()]
        );

        $request->merge(
            ['password' => Hash::make($request->password)]
        );     //  dd($request->all()); 
        $user= User::create($request->all());

        $user
        ->roles()
        ->attach(Role::where('name', 'user')->first());

        Toastr::success('Se ha agregado un usuario!','Buen trabajo'); 
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $request->user()->authorizeRoles(['admin']);


      //dd($request);
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'paise_id' => 'required',
            'ciudade_id' => 'required',
            'academico_id' => 'required',


        ]);



        $email = $request->email;
        if ($email != $user->email) {
          $this->validate($request, [

            'email' => 'required|string|email|max:255|unique:users',

        ]);
          $user->email = $request->email;
      }



      $password = $request->password;
      if ($password != null) {
          $this->validate($request, [

            'password' => 'required|string|min:6|confirmed',
        ]);

          $user->password = Hash::make($request->password);      

      }



      $user->name = $request->name;
      $user->paise_id = $request->paise_id;
      $user->ciudade_id = $request->ciudade_id;
      $user->academico_id = $request->academico_id;
      $user->update();



             //  dd($request->all()); 


      Toastr::info('Se ha actualizado un usuario!','Buen trabajo'); 
      return redirect()->back();
  }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user, Request $request)
    {
        $request->user()->authorizeRoles(['admin']);
        $user->delete();
        Toastr::error('Se ha eliminado un usuario!','Buen trabajo'); 
        return redirect()->back();
    }


    public function perfil()
    {
        $ciudades = Ciudade::all()->sortBy('name')->pluck('name', 'id')->prepend('Selecione ciudad', '');
        $paises = Paise::all()->sortBy('name')->pluck('name', 'id')->prepend('Selecione pais', '');
        $academicos = Academico::all()->sortBy('name')->pluck('name', 'id')->prepend('Selecione N. Academico', '');

        $users = Auth::user();
        $data['users'] = $users;
        $data['ciudades'] = $ciudades;
        $data['paises'] = $paises;
        $data['academicos'] = $academicos;
        $user = Auth::user();
        return view('users.perfil', $data);

    }


    public function perfil_update(Request $request)
    {
      $user = Auth::user();
      //dd($request);
      $this->validate($request, [
        'name' => 'required|string|max:255',
        'paise_id' => 'required',
        'ciudade_id' => 'required',
        'academico_id' => 'required',


    ]);



      $email = $request->email;


      if ($email != $user->email) {
          $this->validate($request, [

            'email' => 'required|string|email|max:255|unique:users',

        ]);
          $user->email = $request->email;
      }



      $password = $request->password;
      if ($password != null) {
          $this->validate($request, [

            'password' => 'required|string|min:6|confirmed',
        ]);

          $user->password = Hash::make($request->password);      

      }



      $user->name = $request->name;
      $user->paise_id = $request->paise_id;
      $user->ciudade_id = $request->ciudade_id;
      $user->academico_id = $request->academico_id;
      $user->update();



             //  dd($request->all()); 


      Toastr::info('Se ha actualizado su perfil!','Buen trabajo'); 
      return redirect()->back();
  }
}
