<?php

namespace App\Http\Controllers\Auth;

use App\Academico;
use App\Ciudade;
use App\Http\Controllers\Controller;
use App\Paise;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    public function showRegistrationForm()
    {

        $ciudades = Ciudade::all()->sortBy('name')->pluck('name', 'id')->prepend('Selecione ciudad', '');
        $paises = Paise::all()->sortBy('name')->pluck('name', 'id')->prepend('Selecione pais', '');
        $academicos = Academico::all()->sortBy('name')->pluck('name', 'id')->prepend('Selecione N. Academico', '');
        $data['ciudades'] = $ciudades;
        $data['paises'] = $paises;
        $data['academicos'] = $academicos;

        if (property_exists($this, 'registerView')) {
            return view($this->registerView, $data);
        }

        return view('auth.register', $data);
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'paise_id' => 'required',
            'ciudade_id' => 'required',
            'academico_id' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

       $user =  User::create([
        'name' => $data['name'],
        'email' => $data['email'],
        'ciudade_id' => $data['ciudade_id'],
        'paise_id' => $data['paise_id'],
        'academico_id' => $data['academico_id'],
        'password' => Hash::make($data['password']),
    ]);

       $user
       ->roles()
       ->attach(Role::where('name', 'user')->first());

  
       return $user;
   }
}
