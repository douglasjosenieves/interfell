<?php

namespace App\Http\Controllers;

use App\Paise;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Kamaln7\Toastr\Facades\Toastr;

class PaiseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['admin']);
        $paises = Paise::name($request->name)->orderBy('id', 'Desc')->paginate(10);
        $data['paises'] = $paises;


        return view('paises.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->user()->authorizeRoles(['admin']);
        $this->validate($request, [
            'name' => 'required|string|max:255|unique:paises',

        ]);


        $request->merge(
            ['created_at' => Carbon::now()]
        );


        Paise::create($request->all());



        Toastr::success('Se ha agregado un pais!','Buen trabajo'); 
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Paise  $paise
     * @return \Illuminate\Http\Response
     */
    public function show(Paise $paise)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Paise  $paise
     * @return \Illuminate\Http\Response
     */
    public function edit(Paise $paise)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Paise  $paise
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Paise $paise)
    {
        $request->user()->authorizeRoles(['admin']);

        $this->validate($request, [
            'name' => 'required|string|max:255|unique:paises',

        ]);

        $paise->name = $request->name;
        $paise->update();
        Toastr::info('Se ha actualizado un pais!','Buen trabajo'); 
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Paise  $paise
     * @return \Illuminate\Http\Response
     */
    public function destroy(Paise $paise, Request $request)
    {
        $request->user()->authorizeRoles(['admin']);
        $paise->delete();
        Toastr::error('Se ha eliminado un pais!','Buen trabajo'); 
        return redirect()->back();
    }
}
