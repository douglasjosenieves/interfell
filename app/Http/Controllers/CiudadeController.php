<?php

namespace App\Http\Controllers;

use App\Ciudade;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Kamaln7\Toastr\Facades\Toastr;

class CiudadeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $request->user()->authorizeRoles(['admin']);
        $ciudades = Ciudade::name($request->name)->orderBy('id', 'Desc')->paginate(10);
        $data['ciudades'] = $ciudades;


        return view('ciudades.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->user()->authorizeRoles(['admin']);
         //dd($request);
        $this->validate($request, [
            'name' => 'required|string|max:255|unique:ciudades',

        ]);


        $request->merge(
            ['created_at' => Carbon::now()]
        );


        Ciudade::create($request->all());



        Toastr::success('Se ha agregado una ciudad!','Buen trabajo'); 
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ciudade  $ciudade
     * @return \Illuminate\Http\Response
     */
    public function show(Ciudade $ciudade)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ciudade  $ciudade
     * @return \Illuminate\Http\Response
     */
    public function edit(Ciudade $ciudade)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ciudade  $ciudade
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ciudade $ciudade)
    {
        $request->user()->authorizeRoles(['admin']);
        $this->validate($request, [
            'name' => 'required|string|max:255|unique:ciudades',

        ]);

        $ciudade->name = $request->name;
        $ciudade->update();
        Toastr::info('Se ha actualizado una ciudad!','Buen trabajo'); 
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ciudade  $ciudade
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ciudade $ciudade, Request $request)
    {
        $request->user()->authorizeRoles(['admin']);
        $ciudade->delete();
        Toastr::error('Se ha eliminado una ciudad!','Buen trabajo'); 
        return redirect()->back();
    }
}
