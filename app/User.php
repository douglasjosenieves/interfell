<?php

namespace App;

use App\Academico;
use App\Ciudade;
use App\Paise;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
  use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'name', 'email', 'password', 'ciudade_id','paise_id', 'academico_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
      'password', 'remember_token',
    ];



    public function scopeName($query, $data)
    {
     $query->where('name','like','%'.$data.'%');
     return $query;
   }

   public function ciudade()
   {
    return $this->belongsTo(Ciudade::class);
  }

  public function paise()
  {
    return $this->belongsTo(Paise::class);
  }

  public function academico()
  {
    return $this->belongsTo(Academico::class);
  }


  public static function totalUsers()
  {

    $count = User::count();
    return $count;

  }


  public static function totalCiudades()
  {

    $count = Ciudade::count();
    return $count;

  }


  public static function totalPaises()
  {

    $count = Paise::count();
    return $count;

  }



  public static function totalAcademicos()
  {

    $count = Academico::count();
    return $count;

  }


  public function roles()
  {
    return $this
    ->belongsToMany(Role::class)
    ->withTimestamps();
  }

  public function cliente()
  {
    return $this->hasOne(Cliente::class,'usuario_id','id');
  }


  public function authorizeRoles($roles)
  {
    if ($this->hasAnyRole($roles)) {
      return true;
    }
    abort(401, 'Esta acción no está autorizada.');
  }
  public function hasAnyRole($roles)
  {
    if (is_array($roles)) {
      foreach ($roles as $role) {
        if ($this->hasRole($role)) {
          return true;
        }
      }
    } else {
      if ($this->hasRole($roles)) {
        return true;
      }
    }
    return false;
  }
  public function hasRole($role)
  {
    if ($this->roles()->where('name', $role)->first()) {
      return true;
    }
    return false;
  }

  public function isAdmin() {

    return $this->hasRole('admin'); // ?? something like this! should return true or false
  }

}
