 

## DESARROLLADO EN LARAVEL

Tarea de pueba

Estimado participante, a continuación la prueba técnica para evaluar su perfil de conocimiento:
Se requiere el desarrollo de un sistema de administración con un pequeño sistema de autentificación -login- que guarde la información de usuarios (email, contraseña, nombre, apellido, ciudad, país, nivel académico, dirección) donde los usuarios también puedan loguearse, muestre la información de perfil y pueda editarla. Que los datos (ciudad, país, nivel académico) sean dinámicos y vengan de la base de datos.
Construya una base de datos que sea relacional y la conexión entre backend y frontend debe ser por ajax (con los POST y GET necesarios)
El desarrollo de la aplicación se deberá realizar utilizando PHP, MySQL y AngularJS (2 en adelante) o cualquier framework JS (react, vue)

Nota: Enviar el proyecto y la base de Datos en un .zip o .tar al correo electrónico reclutamiento@interfell.com. tienes 24 horas para realizarlo.


## DESARROLLADO EN LARAVEL

- Clonar el repositorio.
- Clonar el .env y colocar los acceso a la base de datos.

## EJECUTAR ESTOS COMANDO POR CONSOLA SON DE  LARAVEL
- composer install
- php artisan key:generate
- php artisan migrate:fresh --seed



## USUARIO ADMIN QUE SE INSTALA POR DEFECTO (HAY DOS ROLES ADMIN Y USUARIOS NORMALES)
- user: admin@example.com 
- pass: secret




## NOTA PARA LA PRUEBA

- Nos podemos logear por ajax y sin ajax
- Para las peticiones ajax las trabaje con jquery por la simpicidad del desarrollo no me hizo falta usar vuejs



## DEMO EN PRODUCCION 
- http://interfell.tuweblink.com/ 


## GRACIAS ATTE. DOUGLAS NIEVES douglasjosenieves@gmail.com 


