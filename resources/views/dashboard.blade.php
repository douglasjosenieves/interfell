@extends('layouts.backend.default')

@section('title','Inicio')

@section('content')

<div class="row">

    @if(Auth::user()->hasRole('admin'))
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="card card-stats">
                                <div class="card-header" data-background-color="orange">
                                    <i class="material-icons">assignment_ind</i>
                                </div>
                                <div class="card-content">
                                    <p class="category">Usuarios</p>
                                    <h3 class="card-title">{{app\User::totalUsers()}}</h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="material-icons text-danger"></i>
                                        <a href="#pablo"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="card card-stats">
                                <div class="card-header" data-background-color="rose">
                                    <i class="material-icons">location_on</i>
                                </div>
                                <div class="card-content">
                                    <p class="category">Ciudades</p>
                                    <h3 class="card-title">{{app\User::totalCiudades()}}</h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                   
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="card card-stats">
                                <div class="card-header" data-background-color="green">
                                    <i class="material-icons">public</i>
                                </div>
                                <div class="card-content">
                                    <p class="category">Paises</p>
                                    <h3 class="card-title">{{app\User::totalPaises()}}</h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                               
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="card card-stats">
                                <div class="card-header" data-background-color="blue">
                                  <i class="material-icons">school</i>
                                </div>
                                <div class="card-content">
                                    <p class="category">N. Academicos</p>
                                    <h3 class="card-title">{{app\User::totalAcademicos()}}</h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="material-icons">update</i> Just Updated
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    @else

                       <div class="card">
        <div class="card-header">Bienvenido/a</div>
        <div class="card-body">
          <div class="card-content">
<h2>Bienvenido/a {{Auth::user()->name}}</h2>
</div>
</div>
</div>
                    @endif




@endsection


@section('script')


@endsection
