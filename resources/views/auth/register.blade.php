@extends('layouts.backend.reg-log')
@section('title','Register')

@section('content')
<div class="container">
    <div class="row justify-content-center">
       <div class="col-md-2"></div>
       <div class="col-md-8">
        <div class="card">
         <div class="card-header text-center" data-background-color="green">
            <h4 class="card-title">Register</h4>

        </div>
        <br>

        <div class="card-body">

            <form method="POST" action="{{ route('register') }}">
                @csrf

                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                        @if ($errors->has('name'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>



                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>


                <div class="form-group row">
                   <div class="col-md-12">

                      <div class="form-group{{ $errors->has('paise_id') ? ' has-error' : '' }}">
                        {!! Form::label('paise_id', 'Pais') !!}
                        {!! Form::select('paise_id',$paises, null, ['id' => 'paise_id', 'class' => 'form-control', 'required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('paise_id') }}</small>
                    </div>

                </div> 
            </div> 


            <div class="form-group row">
                <div class="col-md-12">

                  <div class="form-group{{ $errors->has('ciudade_id') ? ' has-error' : '' }}">
                    {!! Form::label('ciudade_id', 'Ciudad') !!}
                    {!! Form::select('ciudade_id',$ciudades, null, ['id' => 'ciudade_id', 'class' => 'form-control', 'required' => 'required']) !!}
                    <small class="text-danger">{{ $errors->first('ciudade_id') }}</small>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-12"><div class="form-group{{ $errors->has('academico_id') ? ' has-error' : '' }}">
              {!! Form::label('academico_id', 'N.Academico') !!}
              {!! Form::select('academico_id',$academicos, null, ['id' => 'academico_id', 'class' => 'form-control', 'required' => 'required']) !!}
              <small class="text-danger">{{ $errors->first('academico_id') }}</small>
          </div>

      </div>
  </div>
  <div class="form-group row">
    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

    <div class="col-md-6">
        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

        @if ($errors->has('password'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

    <div class="col-md-6">
        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
    </div>
</div>

<div class="form-group row mb-0">
    <div class="col-md-6 offset-md-4">
        <button type="submit" class="btn btn-success">
            {{ __('Register') }}
        </button>
    </div>
</div>
</form>
</div>
</div>
</div>
</div>
</div>
@endsection
