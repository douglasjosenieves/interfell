@extends('layouts.backend.default')
@section('title','Usuarios')
{{-- expr --}}

@section('content')

<div class="content">
  <div class="row justify-content-center">


    <div class="row">
      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
    </div>






    <div class="col-md-12">


      <div class="card">
        <div class="card-header">Crear @yield('title')</div>
        <div class="card-body">
          <div class="card-content">
            <a href="#ver" class="">Lista de @yield('title')</a>

            {!! Form::open(['method' => 'POST', 'route' => 'users.store', 'class' => 'form-horizontal']) !!}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
              {!! Form::label('name', 'Nombre') !!}
              {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
              <small class="text-danger">{{ $errors->first('name') }}</small>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              {!! Form::label('email', 'Email address') !!}
              {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'eg: foo@bar.com']) !!}
              <small class="text-danger">{{ $errors->first('email') }}</small>
            </div>


            <div class="row">
             <div class="col-md-4">

              <div class="form-group{{ $errors->has('paise_id') ? ' has-error' : '' }}">
                {!! Form::label('paise_id', 'Pais') !!}
                {!! Form::select('paise_id',$paises, null, ['id' => 'paise_id', 'class' => 'form-control', 'required' => 'required']) !!}
                <small class="text-danger">{{ $errors->first('paise_id') }}</small>
              </div>

            </div> 
            <div class="col-md-4">

              <div class="form-group{{ $errors->has('ciudade_id') ? ' has-error' : '' }}">
                {!! Form::label('ciudade_id', 'Ciudad') !!}
                {!! Form::select('ciudade_id',$ciudades, null, ['id' => 'ciudade_id', 'class' => 'form-control', 'required' => 'required']) !!}
                <small class="text-danger">{{ $errors->first('ciudade_id') }}</small>
              </div>
            </div>

            <div class="col-md-4"><div class="form-group{{ $errors->has('academico_id') ? ' has-error' : '' }}">
              {!! Form::label('academico_id', 'N.Academico') !!}
              {!! Form::select('academico_id',$academicos, null, ['id' => 'academico_id', 'class' => 'form-control', 'required' => 'required']) !!}
              <small class="text-danger">{{ $errors->first('academico_id') }}</small>
            </div></div>

          </div>

          <div class="row">

            <div class="col-md-6">

              <div class="col-md-6">
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  {!! Form::label('password', 'Password') !!}
                  {!! Form::password('password', ['class' => 'form-control', 'required' => 'required']) !!}
                  <small class="text-danger">{{ $errors->first('password') }}</small>
                </div>

              </div>
              <div class="col-md-6">

                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                  {!! Form::label('password_confirmation', 'Password confirmar') !!}
                  {!! Form::password('password_confirmation', ['class' => 'form-control', 'required' => 'required']) !!}
                  <small class="text-danger">{{ $errors->first('password_confirmation') }}</small>
                </div>
              </div>
            </div>
          </div>

          <div class="btn-group pull-right">
            {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
            {!! Form::submit("Add", ['class' => 'btn btn-success']) !!}
          </div>

          {!! Form::close() !!}


        </div>
      </div>
    </div>



    <div class="card" id="ver">
      <div class="card-header" id="lista">Lista de @yield('title')</div>


      <div class="card-body">

        <div class="card-content">


          @component('components/buscador')
          @slot('modulo', 'users')
          @slot('buscarpor', 'Nombre')
          @slot('input', 'name')
          @endcomponent

          <table id="example" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Pais</th>
                <th>Ciudad</th>

                <th>N.Academico</th>

                <th>Creado</th>
                <th>Acción</th>



              </tr>
            </thead>

            <tbody>
              @foreach ($users as $element)


              <tr>
                <td>{{$element->id}}</td>
                <td>{{$element->name}}</td>
                <td>{{$element->paise->name}}</td>
                <td>{{$element->ciudade->name}}</td>

                <td>{{$element->academico->name}}</td>


                <td>{{$element->created_at->format('y-m-d')}}</td>

                <td width="120px">

                  @component('components/edit_delete')
                  @slot('urldelete', 'users')
                  @slot('id', $element->id )
                  @endcomponent



                </td>



              </tr>

              <!-- Classic Modal -->
              <div class="modal fade" id="myModal{{$element->id}}" tabindex="-1" regla="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="nc-icon nc-simple-remove"></i>
                      </button>
                      <h4 class="modal-title">{{$element->name}}</h4>
                    </div>
                    <div class="modal-body">



                      {!! Form::open(['method' => 'PUT', 'route' => ['users.update',$element->id], 'class' => 'form-horizontal']) !!}

                      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        {!! Form::label('name', 'Nombre') !!}
                        {!! Form::text('name', $element->name, ['class' => 'form-control', 'required' => 'required']) !!}
                        <small class="text-danger">{{ $errors->first('name') }}</small>
                      </div>

                      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {!! Form::label('email', 'Email address') !!}
                        {!! Form::email('email', $element->email, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'eg: foo@bar.com']) !!}
                        <small class="text-danger">{{ $errors->first('email') }}</small>
                      </div>


                      <div class="row">
                        <div class="col-md-4">

                          <div class="form-group{{ $errors->has('paise_id') ? ' has-error' : '' }}">
                            {!! Form::label('paise_id', 'Pais') !!}
                            {!! Form::select('paise_id',$paises, $element->paise_id, ['id' => 'paise_id', 'class' => 'form-control', 'required' => 'required']) !!}
                            <small class="text-danger">{{ $errors->first('paise_id') }}</small>
                          </div>

                        </div>  
                        <div class="col-md-4">

                          <div class="form-group{{ $errors->has('ciudade_id') ? ' has-error' : '' }}">
                            {!! Form::label('ciudade_id', 'Ciudad') !!}
                            {!! Form::select('ciudade_id',$ciudades, $element->ciudade_id, ['id' => 'ciudade_id', 'class' => 'form-control', 'required' => 'required']) !!}
                            <small class="text-danger">{{ $errors->first('ciudade_id') }}</small>
                          </div>
                        </div>

                        <div class="col-md-4"><div class="form-group{{ $errors->has('academico_id') ? ' has-error' : '' }}">
                          {!! Form::label('academico_id', 'N.Academico') !!}
                          {!! Form::select('academico_id',$academicos, $element->academico_id, ['id' => 'academico_id', 'class' => 'form-control', 'required' => 'required']) !!}
                          <small class="text-danger">{{ $errors->first('academico_id') }}</small>
                        </div></div>

                      </div>

 <div class="row">

            <div class="col-md-12">

              <div class="col-md-6">
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  {!! Form::label('password', 'Password') !!}
                  {!! Form::password('password', ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('password') }}</small>
                </div>

              </div>
              <div class="col-md-6">

                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                  {!! Form::label('password_confirmation', 'Password confirmar') !!}
                  {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('password_confirmation') }}</small>
                </div>
              </div>
            </div>
          </div>



                    </div>


                    <div class="modal-footer">
                      <div class="left-side">
                       {!! Form::reset("Reset", ['class' => 'btn btn-default btn-link']) !!}
                     </div>
                     <div class="divider"></div>
                     <div class="right-side">
                      {!! Form::submit("Editar", ['class' => 'btn btn-warning btn-link']) !!}
                      {!! Form::close() !!}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!--  End Modal -->
            @endforeach


          </tbody>
        </table>

        {{ $users->links() }}
      </div>

    </div>
  </div>
</div>
</div>
</div>
@endsection

@section('script')
{{-- expr --}}
@endsection