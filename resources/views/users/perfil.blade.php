@extends('layouts.backend.default')
@section('title','Mi perfil')
{{-- expr --}}

@section('content')

<div class="content">
  <div class="row justify-content-center">


    <div class="row">
      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
    </div>






    <div class="col-md-12">


      <div class="card">
        <div class="card-header">Editar @yield('title')</div>
        <div class="card-body">
          <div class="card-content">
   

            {!! Form::open(['method' => 'POST', 'route' => ['users.perfil_update'], 'class' => 'form-horizontal']) !!}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
              {!! Form::label('name', 'Nombre') !!}
              {!! Form::text('name', $users->name, ['class' => 'form-control', 'required' => 'required']) !!}
              <small class="text-danger">{{ $errors->first('name') }}</small>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              {!! Form::label('email', 'Email address') !!}
              {!! Form::email('email', $users->email, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'eg: foo@bar.com']) !!}
              <small class="text-danger">{{ $errors->first('email') }}</small>
            </div>


            <div class="row">
             <div class="col-md-4">

              <div class="form-group{{ $errors->has('paise_id') ? ' has-error' : '' }}">
                {!! Form::label('paise_id', 'Pais') !!}
                {!! Form::select('paise_id',$paises, $users->paise_id, ['id' => 'paise_id', 'class' => 'form-control', 'required' => 'required']) !!}
                <small class="text-danger">{{ $errors->first('paise_id') }}</small>
              </div>

            </div> 
            <div class="col-md-4">

              <div class="form-group{{ $errors->has('ciudade_id') ? ' has-error' : '' }}">
                {!! Form::label('ciudade_id', 'Ciudad') !!}
                {!! Form::select('ciudade_id',$ciudades, $users->ciudade_id, ['id' => 'ciudade_id', 'class' => 'form-control', 'required' => 'required']) !!}
                <small class="text-danger">{{ $errors->first('ciudade_id') }}</small>
              </div>
            </div>

            <div class="col-md-4"><div class="form-group{{ $errors->has('academico_id') ? ' has-error' : '' }}">
              {!! Form::label('academico_id', 'N.Academico') !!}
              {!! Form::select('academico_id',$academicos, $users->academico_id, ['id' => 'academico_id', 'class' => 'form-control', 'required' => 'required']) !!}
              <small class="text-danger">{{ $errors->first('academico_id') }}</small>
            </div></div>

          </div>

          <div class="row">

            <div class="col-md-6">

              <div class="col-md-6">
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                  {!! Form::label('password', 'Password') !!}
                  {!! Form::password('password', ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('password') }}</small>
                </div>

              </div>
              <div class="col-md-6">

                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                  {!! Form::label('password_confirmation', 'Password confirmar') !!}
                  {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                  <small class="text-danger">{{ $errors->first('password_confirmation') }}</small>
                </div>
              </div>
            </div>
          </div>

          <div class="btn-group pull-right">
            {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
            {!! Form::submit("Add", ['class' => 'btn btn-success']) !!}
          </div>

          {!! Form::close() !!}


        </div>
      </div>
    </div>



  
</div>
</div>
</div>
@endsection

@section('script')
{{-- expr --}}
@endsection