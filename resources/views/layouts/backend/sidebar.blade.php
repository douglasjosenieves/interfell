  <div class="sidebar" data-active-color="rose" data-background-color="black" data-image="{{ asset('theme2/assets/img/sidebar-1.jpg') }}">
            <!--
        Tip 1: You can change the color of active element of the sidebar using: data-active-color="purple | blue | green | orange | red | rose"
        Tip 2: you can also add an image using data-image tag
        Tip 3: you can change the color of the sidebar with data-background-color="white | black"
    -->
    <div class="logo">
        <center>
         <img src="{{ asset('images/logo.png') }}" width="40%">
     </center>

 </div>
 <div class="sidebar-wrapper">
    <div class="user">
        <div class="photo">
            <img src="{{ Avatar::create(Auth::user()->name )->toBase64() }}" />
        </div>
        <div class="info">
            <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                <span>
                    {{ Auth::user()->name }}
                    <b class="caret"></b>
                </span>
            </a>
            <div class="clearfix"></div>


            <div class="collapse" id="collapseExample">
                <ul class="nav">
                 



                <li>
                    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    <span class="sidebar-mini"> CS </span>

                    <span class="sidebar-normal"> Cerrar Sesion </span>
                </a>



            </li>
        </ul>
    </div>
</div>
</div>
<ul class="nav">

    
@if(Auth::user()->hasRole('admin'))
    {{-- true expr --}}


    @foreach (Config('app.menu') as $element)
   


    <li>
        <a href="{{ $element['url'] }}">
            <i class="material-icons">{{ $element['icon'] }}</i>
            <p> {{ $element['name'] }} </p>
        </a>
    </li>


    @endforeach

@else
 


   @foreach (Config('app.invitado') as $element)
   


    <li>
        <a href="{{ $element['url'] }}">
            <i class="material-icons">{{ $element['icon'] }}</i>
            <p> {{ $element['name'] }} </p>
        </a>
    </li>


    @endforeach

@endif
   <li>
        <a href="{{ url('/') }}" >
        <i class="material-icons">home</i>
        <p> Frontend </p>
    </a>

    <li>
        <a href="{{ route('logout') }}" onclick="event.preventDefault();
        document.getElementById('logout-form').submit();">
        <i class="material-icons">exit_to_app</i>
        <p> Cerrar Sesion </p>
    </a>


    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        @csrf
    </form>
</li>



 

  
</li>
</ul>
</div>
</div>