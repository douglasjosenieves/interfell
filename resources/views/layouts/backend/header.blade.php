 <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-minimize">
                        <button id="minimizeSidebar" class="btn btn-round btn-white btn-fill btn-just-icon">
                            <i class="material-icons visible-on-sidebar-regular">more_vert</i>
                            <i class="material-icons visible-on-sidebar-mini">view_list</i>
                        </button>
                    </div>
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"> @yield('title') </a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">dashboard</i>
                                    <p class="hidden-lg hidden-md">Dashboard</p>
                                </a>
                            </li>

                             
                               {{--        <li>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    
                                    <p class="">Rastreo / Pre Alerta</p>
                                </a>
                            </li>  

                                           <li>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    
                                    <p class="">Calculador</p>
                                </a>
                            </li> 
                 <li>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    
                                    <p class="">Tarifas</p>
                                </a>
                            </li> 

                                 <li>
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    
                                    <p class="">Cotizaciones</p>
                                </a>
                            </li> 

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">notifications</i>
                                    <span class="notification">5</span>
                                    <p class="hidden-lg hidden-md">
                                        Notifications
                                        <b class="caret"></b>
                                    </p>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="#">Mike John responded to your email</a>
                                    </li>
                                    <li>
                                        <a href="#">You have 5 new tasks</a>
                                    </li>
                                    <li>
                                        <a href="#">You're now friend with Andrew</a>
                                    </li>
                                    <li>
                                        <a href="#">Another Notification</a>
                                    </li>
                                    <li>
                                        <a href="#">Another One</a>
                                    </li>
                                </ul>
                            </li> --}}

                      {{--       <li>

                                
                                <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">person</i>
                                    <p class="hidden-lg hidden-md">Profile</p>
                                </a>
                            </li> --}}
                            <li class="separator hidden-lg hidden-md"></li>
                        </ul>

                        @if(Auth::user()->hasRole('admin'))
                        <form class="navbar-form navbar-right" method="GET" action="{{ url('users/') }}" role="search">
                            <div class="form-group form-search is-empty">
                                <input type="text" name="name" class="form-control" placeholder=" Usuarios ">
                                <span class="material-input"></span>
                            </div>
                            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                                <i class="material-icons">search</i>
                                <div class="ripple-container"></div>
                            </button>
                        </form>

                        @endif
                    </div>
                </div>
            </nav>