@extends('layouts.backend.default')

@section('title','Acceso no autorizado')

@section('content')
 
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="orange">
                                    <i class="material-icons">warning</i>
                                </div>
                                <div class="card-content">

<center>
                                 <h2><i class="fa fa-lock"></i> Acceso no autorizado!</h2>

                                 </center>   
                          </div>
                      </div>
                  </div>
                  </div>

       
@endsection


