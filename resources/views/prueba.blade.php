@extends('layouts.backend.default')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Form</div>

                <div class="card-body">
          
          {!! Form::open(['method' => 'POST', 'url' => '/home', 'class' => 'form-horizontal']) !!}
          
              <div class="form-group{{ $errors->has('nae') ? ' has-error' : '' }}">
                  {!! Form::label('nae', 'Name') !!}
                  {!! Form::text('nae', null, ['class' => 'form-control', 'required' => 'required']) !!}
                  <small class="text-danger">{{ $errors->first('nae') }}</small>
              </div>
          
              <div class="btn-group pull-right">
                  {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
                  {!! Form::submit("Add", ['class' => 'btn btn-success']) !!}
              </div>
          
          {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
