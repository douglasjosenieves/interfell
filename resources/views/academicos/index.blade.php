@extends('layouts.backend.default')
@section('title','Niveles Academicos')
{{-- expr --}}

@section('content')

<div class="content">
  <div class="row justify-content-center">


    <div class="row">
      @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      @if (Session::has('message'))
      <div class="alert alert-info">{{ Session::get('message') }}</div>
      @endif
    </div>






    <div class="col-md-12">


      <div class="card">
        <div class="card-header">Crear @yield('title')</div>
        <div class="card-body">
          <div class="card-content">
            <a href="#ver" class="">Lista de @yield('title')</a>

            {!! Form::open(['method' => 'POST', 'route' => 'academicos.store', 'class' => 'form-horizontal']) !!}

            
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
              {!! Form::label('name', 'Nombre') !!}
              {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
              <small class="text-danger">{{ $errors->first('name') }}</small>
            </div>


          </div>

          

          <div class="btn-group pull-right">
            {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
            {!! Form::submit("Add", ['class' => 'btn btn-success']) !!}
          </div>

          {!! Form::close() !!}


        </div>
      </div>
    </div>



    <div class="card" id="ver">
      <div class="card-header" id="lista">Lista de @yield('title')</div>


      <div class="card-body">

        <div class="card-content">


          @component('components/buscador')
          @slot('modulo', 'academicos')
          @slot('buscarpor', 'Nombre')
          @slot('input', 'name')
          @endcomponent

          <table id="example" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
              <tr>
                <th>Id</th>
                <th>Nombre</th>
                

                <th>Creado</th>
                <th>Acción</th>



              </tr>
            </thead>

            <tbody>
              @foreach ($academicos as $element)


              <tr>
                <td>{{$element->id}}</td>
                <td>{{$element->name}}</td>
                


                <td>{{$element->created_at->format('y-m-d')}}</td>

                <td width="120px">

                  @component('components/edit_delete')
                  @slot('urldelete', 'academicos')
                  @slot('id', $element->id )
                  @endcomponent



                </td>



              </tr>

              <!-- Classic Modal -->
              <div class="modal fade" id="myModal{{$element->id}}" tabindex="-1" regla="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="nc-icon nc-simple-remove"></i>
                      </button>
                      <h4 class="modal-title">{{$element->name}}</h4>
                    </div>
                    <div class="modal-body">



                      {!! Form::open(['method' => 'PUT', 'route' => ['academicos.update',$element->id], 'class' => 'form-horizontal']) !!}

                      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                       {!! Form::label('name', 'Nombre') !!}
                       {!! Form::text('name',$element->name, ['class' => 'form-control', 'required' => 'required']) !!}
                       <small class="text-danger">{{ $errors->first('name') }}</small>
                     </div>

                   </div>

                   

                   <div class="modal-footer">
                    <div class="left-side">
                     {!! Form::reset("Reset", ['class' => 'btn btn-default btn-link']) !!}
                   </div>
                   <div class="divider"></div>
                   <div class="right-side">
                    {!! Form::submit("Editar", ['class' => 'btn btn-warning btn-link']) !!}
                    {!! Form::close() !!}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!--  End Modal -->
          @endforeach


        </tbody>
      </table>

      {{ $academicos->links() }}
    </div>

  </div>
</div>
</div>
</div>
</div>
@endsection

@section('script')
{{-- expr --}}
@endsection