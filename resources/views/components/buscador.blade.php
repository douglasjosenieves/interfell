
{!! Form::open(['method' => 'GET', 'route' => $modulo.'.index', 'class' => 'form-horizontal']) !!}
<div class="input-group">
  <div class="form-group{{ $errors->has($input) ? ' has-error' : '' }}">

    {!! Form::text($input, null, ['class' => 'form-control', 'required' => 'required', 'placeholder'=> 'Buscar por '.$buscarpor.'...']) !!}
    <small class="text-danger">{{ $errors->first($input) }}</small>
  </div>
  <span style="margin-top: -8px;" class="input-group-btn">
    <button type="submit"  style="margin: 0px 0px;" class="btn btn-success" type="button">Go!</button>
    <a class="btn btn-default" style="margin: 0px 0px;" href="{{ url('/'.$modulo) }}">Reset</a>
  </span>        
</div><!-- /input-group -->
{!! Form::close() !!}