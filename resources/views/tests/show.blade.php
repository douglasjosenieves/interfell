@extends('layouts.backend.default')

@section('title','Tests')
@section('content')
               <div class="row">
                @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

                    <div class="col-12">
                        <div class="card card-outline-info">
                            <div class="card-header">
                                 <a href="{{ URL::previous() }}" class="btn btn-xs btn-default"><i class="fa fa-arrow-left"></i> Atras</a> 
                                <h4 class="m-b-0 text-white">Ver @yield('title')</h4>
                            </div>
                             <div class="card-body" style="padding: 10px">
                                <form action="{{ url('test') }}" method="post">
                                  {{ csrf_field() }}
                                    <div class="form-body">
                                   
                                        <hr>
                                        <div class="row p-t-20">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Name</label>
                                                    <input type="text" name="name" id="name" class="form-control" placeholder="" value="{{$test->name}}">
                                                    <small class="form-control-feedback"> This is inline help </small> </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                              <div class="form-group">
                                                    <label class="control-label">Description</label>
                                                    <input type="text" name="description" id="description" class="form-control" placeholder="" value="{{$test->description}}" >
                                                    <small class="form-control-feedback"> This is inline help </small> </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->

                                             <div class="row p-t-20">
                                                   <!--/span-->
                                            <div class="col-md-6">
                                              <div class="form-group">
                                                    <label class="control-label">Tel</label>
                                                    <input type="text" name="tel" id="tel" class="form-control" placeholder="" value=" {{$test->tel}}">
                                                    <small class="form-control-feedback"> This is inline help </small> </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        
                                
                                    </div>
                              
                                </form>
                            </div>
                        </div>
                </div>
    {{-- expr --}}
@endsection