@extends('layouts.backend.default')

@section('title','Tests')
@section('content')
               <div class="row">
                @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

                    <div class="col-12">
                        <div class="card card-outline-info">
                            <div class="card-header">
                                 <a href="{{ URL::previous() }}" class="btn btn-xs btn-default"><i class="fa fa-arrow-left"></i> Atras</a> 
                                  <h4 class="m-b-0 text-white">Editar @yield('title')</h4>
                            </div>
                           <div class="card-body" style="padding: 10px">
                                <form action="{{ url('tests/'.$test->id) }}" method="POST">
                                  {{ method_field('PUT') }}
                                  {{ csrf_field() }}
                                <div class="form-body">
                                
                                        <hr>
                                        <div class="row p-t-20">
                                            <div class="col-md-6">
                                                <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                                                    <label class="control-label">Name</label>
                                                    <input type="text" name="name" id="name" class="form-control" placeholder="" value="{{$test->name}}" >
                                                    <small class="text-danger"> {{ $errors->first('name') }}</small> </div>
                                            </div>
                                            <!--/span-->
                                            <div class="col-md-6">
                                              <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                                                    <label class="control-label">Description</label>
                                                    <input type="text" name="description" id="description" class="form-control" placeholder="" required  value="{{$test->description}}">
                                                    <small class="text-danger"> {{ $errors->first('description') }}</small> </div>
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->

                                             <div class="row p-t-20">
                                                   <!--/span-->
                                            <div class="col-md-6">
                                              <div class="form-group {{ $errors->has('tel') ? ' has-error' : '' }}">
                                                    <label class="control-label">Tel</label>
                                                    <input type="text" name="tel" id="tel" class="form-control" placeholder="" required value="{{$test->tel}}">
                                                    <small class="text-danger"> {{ $errors->first('tel') }}</small> </div> 
                                            </div>
                                            <!--/span-->
                                        </div>
                                        <!--/row-->
                                        
                                
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                        <button type="button" class="btn btn-inverse">Cancel</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                </div>
    {{-- expr --}}
@endsection
