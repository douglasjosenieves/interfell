@extends('layouts.backend.default')

@section('title','Tests')
@section('content')
               <div class="row">
                    <div class="col-12">
                       <!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

                        <div class="card">
                            <div class="card-body" style="padding: 10px">
                                <a href="{{ url('tests/create') }}" class="btn btn-xs btn-success"><i class="fa fa-plus"></i> Nuevo</a>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                             
                               <div class="table-responsive m-t-40">
                                    <table id="example" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                  <th>Id</th>
                                                <th>Name</th>
                                             
                                                  <th>Tel</th>
                                                <th>Acción</th>
                                              
                                            </tr>
                                        </thead>
                                        <tfoot>
                                            <tr>
                                                 <th>Id</th>
                                                <th>Name</th>
                                            
                                                      <th>Tel</th>
                                                 <th>Acción</th>
                                            
                                                
                                            </tr>
                                        </tfoot>
                                        <tbody>
                                       @foreach ($tests as $element)
     

                                            <tr>
                                                  <td>{{$element->id}}</td>
                                                <td>{{$element->name}}</td>
                                               
                                                   <td>{{$element->tel}}</td>
        <td>
        <a  href="{{ url('tests/'.$element->id) }}"  title="Editar" class="btn btn-xs btn-primary btn-circle"><i class="fa fa-eye"></i> </a>
        <a  href="{{ url('tests/'.$element->id.'/edit') }}"  title="Editar" class="btn btn-xs btn-warning btn-circle"><i class="fa fa-edit"></i> </a>
             

<form action="{{ url('tests/'.$element->id) }}" method="POST" style="display: inline;">
     {{ method_field('DELETE') }}
   {{ csrf_field() }}
    
<button type="submit" onclick="return confirm('Estás seguro de que deseas eliminar el registro?')" class="btn btn-xs btn-danger btn-circle"> <i class="fa fa-close"></i> </button>
</form>

                                                   </td>
                                            </tr>
                                          @endforeach
                                           
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                       
                     
                </div>
	{{-- expr --}}
@endsection
@section('script')

 
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
       <script>
   $(document).ready(function() {
       
        $(document).ready(function() {
            var table = $('#example').DataTable({
                 dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]

             
                
               
            });
            // Order by the grouping
          
        });
    });

    </script>
@endsection

 