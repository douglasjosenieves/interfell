<?php

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
Route::get('/', function () {
	return view('index');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home'); 

Route::get('/admin', 'HomeController@index')->name('home'); 

Route::resource('users', 'UserController');
Route::resource('ciudades', 'CiudadeController');
Route::resource('paises', 'PaiseController');
Route::resource('academicos', 'AcademicoController');


Route::get('perfil', 'UserController@perfil');
Route::post('perfil_update', 'UserController@perfil_update')->name('users.perfil_update');



Route::post('login_ajax', function(Request $request) {
	$credentials = $request->only('email', 'password');

	if (Auth::attempt($credentials)) {
		$user = User::where('email', $request->email)->first();
		Auth::loginUsingId($user->id);
	      return response()->json([
            'auth' => auth()->check(),
            'user' => $user,
        
        ]);
	}

	else {

		   return response()->json([
            'auth' => 'false',

        
        ]);
	}
	
});